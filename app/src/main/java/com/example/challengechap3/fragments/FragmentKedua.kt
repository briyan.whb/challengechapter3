package com.example.challengechap3.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.challengechap3.data.User
import com.example.challengechap3.databinding.FragmentKeduaBinding

class FragmentKedua : Fragment() {

    private var _binding : FragmentKeduaBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentKeduaBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnscreen1.setOnClickListener {
            val nama = binding.editText.text.toString()
            val user = User(nama)

            val action = FragmentKeduaDirections.actionFragmentKeduaToFragmentKetiga(user)
            findNavController().navigate(action)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}