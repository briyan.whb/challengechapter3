package com.example.challengechap3.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.challengechap3.data.User
import com.example.challengechap3.databinding.FragmentKetigaBinding

class FragmentKetiga : Fragment() {

    private var _binding : FragmentKetigaBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<FragmentKetigaArgs>()

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentKetigaBinding.inflate(inflater, container, false)
        cekUsia()
        binding.data1.text = "${args.user3.nama}"
        binding.data2.text = "${args.user3.usia}"
        binding.data3.text = "${args.user3.alamat}"
        binding.data4.text = "${args.user3.pekerjaan}"
        if (binding.data1.text.isNullOrEmpty()){
            binding.data1.visibility = View.GONE
            binding.data2.visibility = View.GONE
            binding.data3.visibility = View.GONE
            binding.data4.visibility = View.GONE
        }
        else if (binding.data2.text.isNullOrEmpty() && binding.data3.text.isNullOrEmpty() && binding.data4.text.isNullOrEmpty()){
            binding.data2.visibility = View.GONE
            binding.data3.visibility = View.GONE
            binding.data4.visibility = View.GONE
        }
        else if (binding.data2.text.isNullOrEmpty() && binding.data3.text.isNullOrEmpty()){
            binding.data2.visibility = View.GONE
            binding.data3.visibility = View.GONE
        }
        else if (binding.data3.text.isNullOrEmpty() && binding.data4.text.isNullOrEmpty()){
            binding.data3.visibility = View.GONE
            binding.data4.visibility = View.GONE
        }
        else if (binding.data4.text.isNullOrEmpty() && binding.data2.text.isNullOrEmpty()){
            binding.data2.visibility = View.GONE
            binding.data4.visibility = View.GONE
        }
        else if (binding.data2.text.isNullOrEmpty()){
            binding.data2.visibility = View.GONE
        }
        else if (binding.data3.text.isNullOrEmpty()){
            binding.data3.visibility = View.GONE
        }
        else if (binding.data4.text.isNullOrEmpty()){
            binding.data4.visibility = View.GONE
        }
        binding.data1.text = "Nama Anda : ${args.user3.nama}"
        binding.data2.text = "Usia Anda : ${args.user3.usia}"
        binding.data3.text = "Alamat Anda : ${args.user3.alamat}"
        binding.data4.text = "Pekerjaaan Anda : ${args.user3.pekerjaan}"
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnscreen1.setOnClickListener {
            val nama : String? = args.user3.nama
            val usia : String? = args.user3.usia
            val alamat : String? = args.user3.alamat
            val pekerjaan : String? = args.user3.pekerjaan
            val user = User(nama, usia, alamat, pekerjaan)

            val action = FragmentKetigaDirections.actionFragmentKetigaToFragmentKeempat(user)
            findNavController().navigate(action)
        }
    }

    private fun cekUsia(){
        val cek = args.user3.usia?.toIntOrNull()

        if (cek != null) {
            args.user3.usia = if (cek % 2 == 0){
                "$cek, bernilai genap"
            } else {
                "$cek, bernilai ganjil"
            }
        }

    }
    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}