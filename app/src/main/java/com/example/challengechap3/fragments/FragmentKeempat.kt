package com.example.challengechap3.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.challengechap3.data.User
import com.example.challengechap3.databinding.FragmentKeempatBinding

class FragmentKeempat : Fragment() {

    private var _binding : FragmentKeempatBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<FragmentKeempatArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentKeempatBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnscreen1.setOnClickListener {
            val nama = args.user4.nama
            val usia = binding.data2.text.toString()
            val alamat = binding.data3.text.toString()
            val pekerjaan = binding.data4.text.toString()

            val user = User(nama, usia, alamat, pekerjaan)

            val action = FragmentKeempatDirections.actionFragmentKeempatToFragmentKetiga(user)
            findNavController().navigate(action)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }


}