package com.example.challengechap3.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.example.challengechap3.R
import com.example.challengechap3.databinding.FragmentFirstBinding

class FragmentFirst : Fragment() {

    private var _binding : FragmentFirstBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnscreen1.setOnClickListener {
            it.findNavController().navigate(R.id.action_fragmentFirst_to_fragmentKedua)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}