package com.example.challengechap3.data

import android.os.Parcel
import android.os.Parcelable

data class User(
    val nama: String? = "",
    var usia: String? = "",
    val alamat: String? = "",
    val pekerjaan: String? = ""
    ) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    )

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(nama)
        parcel.writeString(usia)
        parcel.writeString(alamat)
        parcel.writeString(pekerjaan)
    }

    companion object CREATOR : Parcelable.Creator<User> {
        override fun createFromParcel(parcel: Parcel): User {
            return User(parcel)
        }

        override fun newArray(size: Int): Array<User?> {
            return arrayOfNulls(size)
        }
    }
}